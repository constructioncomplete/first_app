import React from "react";
import "./app.scss"
import state from "../store/index"

const App = () => {
  return (
    <div className="app">
      Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ea impedit,
      consequuntur accusantium esse fugiat similique?
    </div>
  );
};

export default App;
